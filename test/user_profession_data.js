var userProfession = require("../data/user_profession");

var chai = require("chai");
var expect = chai.expect;

describe("user profession", function(){
  beforeEach(function(){
    userProfession.userProfessionId = 1;
    userProfession.userProfessionUserId = 1;
    userProfession.userProfessionProfessionId = 1;
    userProfession.userProfessionDateTime = "3/01/2020";
  });
  it("userProfessionId: setter and getter", function(){
    expect(userProfession.user_profession_id).to.equal(1);
    expect(userProfession.user_profession_id).not.to.equal(2);
  });
  it("userProfessionUserId: setter and getter", function(){
    expect(userProfession.user_id).to.equal(1);
    expect(userProfession.user_id).not.to.equal(2);
  });
  it("userProfessionProfessionId: setter and getter", function(){
    expect(userProfession.profession_id).to.equal(1);
    expect(userProfession.profession_id).not.to.equal(2);
  });
  it("dateTime: setter and getter", function(){
    expect(userProfession.date_time).to.equal("3/01/2020");
    expect(userProfession.date_time).not.to.equal("4/01/2021");
  });
});
