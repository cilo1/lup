var victim = require("../data/victim");

var chai = require("chai");
var expect = chai.expect;

describe("victim", function(){
  beforeEach(function(){
    victim.victimVictimId = 1;
    victim.victimUserId = 1;
    victim.victimDateTime = "3/01/2020";
  });
  it("victimId: setter and getter", function(){
    expect(victim.victim_id).to.equal(1);
    expect(victim.victim_id).not.to.equal(2);
  });
  it("userId: setter and getter", function(){
    expect(victim.user_id).to.equal(1);
    expect(victim.user_id).not.to.equal(2);
  });
  it("dateTime: setter and getter", function(){
    expect(victim.date_time).to.equal("3/01/2020");
    expect(victim.date_time).not.to.equal("4/01/2021");
  });
});
