var appointment = require("../data/appointment");

var chai = require("chai");
var expect = chai.expect;

describe("appointment", function(){
  beforeEach(function(){
    appointment.appointmentId = 1;
    appointment.appointmentUserPairingId = 1;
    appointment.appointmentSchedule = "3/01/2020";
    appointment.appointmentDateTime = "3/01/2020";
  });
  it("appointmentId: setter and getter", function(){
    expect(appointment.appointment_id).to.equal(1);
    expect(appointment.appointment_id).not.to.equal(2);
  });
  it("appointmentUserPairingId: setter and getter", function(){
    expect(appointment.user_pairing_id).to.equal(1);
    expect(appointment.user_pairing_id).not.to.equal(2);
  });
  it("appointmentSchedule: setter and getter", function(){
    expect(appointment.schedule).to.equal("3/01/2020");
    expect(appointment.schedule).not.to.equal("4/01/2020");
  });
  it("dateTime: setter and getter", function(){
    expect(appointment.date_time).to.equal("3/01/2020");
    expect(appointment.date_time).not.to.equal("4/01/2021");
  });
});
