var user = require("../data/user");

var chai = require("chai");
var expect = chai.expect;

describe("user", function(){
  beforeEach(function(){
    user.userUserId = 1;
    user.userFname = "John";
    user.userLname = "Doe";
    user.userEmail = "john@gmail.com";
    user.userPassword = "jdoe";
    user.userPhone = "0723456789";
    user.userActivation = "ok";
    user.userDateTime = "3/01/2020";
  });
  it("userId: setter and getter", function(){
    expect(user.user_id).to.equal(1);
    expect(user.user_id).not.to.equal(2);
  });
  it("firstname: setter and getter", function(){
    expect(user.fname).to.equal("John");
    expect(user.fname).not.to.equal("Jack");
  });
  it("lastname: setter and getter", function(){
    expect(user.lname).to.equal("Doe");
    expect(user.lname).not.to.equal("Jack");
  });
  it("email: setter and getter", function(){
    expect(user.email).to.equal("john@gmail.com");
    expect(user.email).not.to.equal("Jack@email.com");
  });
  it("password: setter and getter", function(){
    expect(user.password).to.equal("jdoe");
    expect(user.password).not.to.equal("Jack");
  });
  it("phone: setter and getter", function(){
    expect(user.phone).to.equal("0723456789");
    expect(user.phone).not.to.equal("07345678");
  });
  it("activation: setter and getter", function(){
    expect(user.activation).to.equal("ok");
    expect(user.activation).not.to.equal("no");
  });
  it("dateTime: setter and getter", function(){
    expect(user.date_time).to.equal("3/01/2020");
    expect(user.date_time).not.to.equal("4/01/2021");
  });
});
