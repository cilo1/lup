var accussed = require("../data/accussed");

var chai = require("chai");
var expect = chai.expect;

describe("accussed", function(){
  beforeEach(function(){
    accussed.accussedAccussedId = 1;
    accussed.accussedUserId = 1;
    accussed.accussedDateTime = "3/01/2020";
  });
  it("accussedId: setter and getter", function(){
    expect(accussed.accussed_id).to.equal(1);
    expect(accussed.accussed_id).not.to.equal(2);
  });
  it("userId: setter and getter", function(){
    expect(accussed.user_id).to.equal(1);
    expect(accussed.user_id).not.to.equal(2);
  });
  it("dateTime: setter and getter", function(){
    expect(accussed.date_time).to.equal("3/01/2020");
    expect(accussed.date_time).not.to.equal("4/01/2021");
  });
});
