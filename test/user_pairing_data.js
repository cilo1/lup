var userPairing = require("../data/user_pairing");

var chai = require("chai");
var expect = chai.expect;

describe("user pairing", function(){
  beforeEach(function(){
    userPairing.userPairingId = 1;
    userPairing.userPairingGroupId = 1;
    userPairing.userPairingPairing = "1,2";
    userPairing.userPairingDateTime = "3/01/2020";
  });
  it("userPairingId: setter and getter", function(){
    expect(userPairing.user_pairing_id).to.equal(1);
    expect(userPairing.user_pairing_id).not.to.equal(2);
  });
  it("userPairingGroupId: setter and getter", function(){
    expect(userPairing.group_id).to.equal(1);
    expect(userPairing.group_id).not.to.equal(2);
  });
  it("userPairingParing: setter and getter", function(){
    expect(userPairing.pairing).to.equal("1,2");
    expect(userPairing.pairing).not.to.equal("3,4");
  });
  it("dateTime: setter and getter", function(){
    expect(userPairing.date_time).to.equal("3/01/2020");
    expect(userPairing.date_time).not.to.equal("4/01/2021");
  });
});
