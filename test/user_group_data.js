var userGroup = require("../data/user_group");

var chai = require("chai");
var expect = chai.expect;

describe("user group", function(){
  beforeEach(function(){
    userGroup.userGroupId = 1;
    userGroup.userGroupUserId = 1;
    userGroup.userGroupGroupId = 1;
    userGroup.userGroupDateTime = "3/01/2020";
  });
  it("userGroupId: setter and getter", function(){
    expect(userGroup.user_group_id).to.equal(1);
    expect(userGroup.user_group_id).not.to.equal(2);
  });
  it("userGroupUserId: setter and getter", function(){
    expect(userGroup.user_id).to.equal(1);
    expect(userGroup.user_id).not.to.equal(2);
  });
  it("userGroupProfessionId: setter and getter", function(){
    expect(userGroup.group_id).to.equal(1);
    expect(userGroup.group_id).not.to.equal(2);
  });
  it("dateTime: setter and getter", function(){
    expect(userGroup.date_time).to.equal("3/01/2020");
    expect(userGroup.date_time).not.to.equal("4/01/2021");
  });
});
