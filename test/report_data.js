var report = require("../data/report");

var chai = require("chai");
var expect = chai.expect;

describe("report", function(){
  beforeEach(function(){
    report.reportReportId = 1;
    report.reportItem = "Abusive";
    report.reportDescription = "dsdsa sadsa sadsa dsad";
    report.reportDateTime = "3/01/2020";
  });
  it("reportId: setter and getter", function(){
    expect(report.report_id).to.equal(1);
    expect(report.report_id).not.to.equal(2);
  });
  it("report name: setter and getter", function(){
    expect(report.item).to.equal("Abusive");
    expect(report.item).not.to.equal("Writing");
  });
  it("report description: setter and getter", function(){
    expect(report.description).to.equal("dsdsa sadsa sadsa dsad");
    expect(report.description).not.to.equal("dsds asdasd sdd");
  });
  it("dateTime: setter and getter", function(){
    expect(report.date_time).to.equal("3/01/2020");
    expect(report.date_time).not.to.equal("4/01/2021");
  });
});
