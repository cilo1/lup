var group = require("../data/group");

var chai = require("chai");
var expect = chai.expect;

describe("group", function(){
  beforeEach(function(){
    group.groupGroupId = 1;
    group.groupProfessionId = 1;
    group.groupName = "Software engineer";
    group.groupDescription = "dsdsa sadsa sadsa dsad";
    group.groupMaxTotal = 8;
    group.groupCurrentTotal = 3;
    group.groupAvailability = 5;
    group.groupDateTime = "3/01/2020";
  });
  it("groupId: setter and getter", function(){
    expect(group.group_id).to.equal(1);
    expect(group.group_id).not.to.equal(2);
  });
  it("professionId: setter and getter", function(){
    expect(group.profession_id).to.equal(1);
    expect(group.profession_id).not.to.equal(2);
  });
  it("group name: setter and getter", function(){
    expect(group.name).to.equal("Software engineer");
    expect(group.name).not.to.equal("Writing");
  });
  it("group description: setter and getter", function(){
    expect(group.description).to.equal("dsdsa sadsa sadsa dsad");
    expect(group.description).not.to.equal("dsds asdasd sdd");
  });
  it("group max_total: setter and getter", function(){
    expect(group.max_total).to.equal(8);
    expect(group.max_total).not.to.equal(7);
  });
  it("group current_total: setter and getter", function(){
    expect(group.current_total).to.equal(3);
    expect(group.current_total).not.to.equal(7);
  });
  it("group availability: setter and getter", function(){
    expect(group.availability).to.equal(5);
    expect(group.availability).not.to.equal(7);
  });
  it("dateTime: setter and getter", function(){
    expect(group.date_time).to.equal("3/01/2020");
    expect(group.date_time).not.to.equal("4/01/2021");
  });
});
