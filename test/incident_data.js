var incident = require("../data/incident");

var chai = require("chai");
var expect = chai.expect;

describe("incident", function(){
  beforeEach(function(){
    incident.incidentIncidentId = 1;
    incident.incidentVictimId = 1;
    incident.incidentAccussedId = 1;
    incident.incidentReportId = 1;
    incident.incidentDateTime = "3/01/2020";
  });
  it("incidentId: setter and getter", function(){
    expect(incident.incident_id).to.equal(1);
    expect(incident.incident_id).not.to.equal(2);
  });
  it("victimId: setter and getter", function(){
    expect(incident.victim_id).to.equal(1);
    expect(incident.victim_id).not.to.equal(2);
  });
  it("accussedId: setter and getter", function(){
    expect(incident.accussed_id).to.equal(1);
    expect(incident.accussed_id).not.to.equal(2);
  });
  it("reportId: setter and getter", function(){
    expect(incident.report_id).to.equal(1);
    expect(incident.report_id).not.to.equal(2);
  });
  it("dateTime: setter and getter", function(){
    expect(incident.date_time).to.equal("3/01/2020");
    expect(incident.date_time).not.to.equal("4/01/2021");
  });
});
