var profession = require("../data/profession");

var chai = require("chai");
var expect = chai.expect;

describe("profession", function(){
  beforeEach(function(){
    profession.professionProfessionId = 1;
    profession.professionName = "Software engineer";
    profession.professionDescription = "dsdsa sadsa sadsa dsad";
    profession.professionDateTime = "3/01/2020";
  });
  it("professionId: setter and getter", function(){
    expect(profession.profession_id).to.equal(1);
    expect(profession.profession_id).not.to.equal(2);
  });
  it("profession name: setter and getter", function(){
    expect(profession.name).to.equal("Software engineer");
    expect(profession.name).not.to.equal("Writing");
  });
  it("profession description: setter and getter", function(){
    expect(profession.description).to.equal("dsdsa sadsa sadsa dsad");
    expect(profession.description).not.to.equal("dsds asdasd sdd");
  });
  it("dateTime: setter and getter", function(){
    expect(profession.date_time).to.equal("3/01/2020");
    expect(profession.date_time).not.to.equal("4/01/2021");
  });
});
