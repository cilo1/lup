var userInterest = require("../data/user_interest");

var chai = require("chai");
var expect = chai.expect;

describe("user interest", function(){
  beforeEach(function(){
    userInterest.userInterestId = 1;
    userInterest.userInterestUserId = 1;
    userInterest.userInterestProfessionId = 1;
    userInterest.userInterestDateTime = "3/01/2020";
  });
  it("userInterestId: setter and getter", function(){
    expect(userInterest.user_interest_id).to.equal(1);
    expect(userInterest.user_interest_id).not.to.equal(2);
  });
  it("userInterestUserId: setter and getter", function(){
    expect(userInterest.user_id).to.equal(1);
    expect(userInterest.user_id).not.to.equal(2);
  });
  it("userInterestProfessionId: setter and getter", function(){
    expect(userInterest.profession_id).to.equal(1);
    expect(userInterest.profession_id).not.to.equal(2);
  });
  it("dateTime: setter and getter", function(){
    expect(userInterest.date_time).to.equal("3/01/2020");
    expect(userInterest.date_time).not.to.equal("4/01/2021");
  });
});
