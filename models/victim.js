var db = require('../databases/mysql_db');
var victim = require('../data/victim');

var query = "";
var values = [];
var dateTime = new Date();

var createVictim = function(new_victim, callback){
  query = "INSERT INTO victims VALUES(null,?,?)";
  values = [
    new_victim.user_id,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateVictim = (victim, callback) => {
  query = "UPDATE victims set user_id=? WHERE victim_id=?";
  values = [
    victim.user_id,
    victim.victim_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getVictim = (victimId, callback) => {
  query = "SELECT * FROM victims WHERE victim_id=?";
  values = [
    victimId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setVictimData(result));
  });
}

var getVictims = (callback) => {
  query = "SELECT * FROM victims";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteVictim = (victimId, callback) => {
  query = "DELETE FROM victims WHERE victim_id=?";
  values = [
    victimId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setVictimData(data){
  victim.victimVictimId = data[0].victim_id;
  victim.victimUserId = data[0].user_id;
  victim.victimDateTime = data[0].date_time;
  return victim;
}

module.exports = {
  createVictim,
  updateVictim,
  getVictim,
  getVictims,
  deleteVictim
}
