var db = require('../databases/mysql_db');
var pwd_encrypt = require('../utils/password_encrypt');
var user = require('../data/user');

var query = "";
var values = [];
var dateTime = new Date();

var createUser = function(new_user, callback){
  query = "INSERT INTO users VALUES(null,?,?,?,?,?,?,?)";
  pwd_encrypt.cryptPassword(new_user.password, function(err, encrypted_pass){
    if(!err)
      values = [
        new_user.fname,
        new_user.lname,
        new_user.email,
        encrypted_pass,
        new_user.phone,
        "no",
        dateTime
      ]
      return passQuery(query, values, callback);
    console.log("encryption failed");
  });
}

var authUser = (acc_user, callback)=> {
  query = "SELECT * FROM users WHERE email=?";
  values = [
    acc_user.email
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("Password: "+result[0].password);
      pwd_encrypt.comparePassword(acc_user.password, result[0].password,
        function(err, isPasswordMatch){
        if(err === null)
          return callback(setUserData(result));
        console.log("Password: No match");
      });

    console.log("No response from db");
  });
}

var updateUser = (acc_user, callback) => {
  query = "UPDATE users set fname=?, lname=?, email=?, phone=?  WHERE user_id=?";
  values = [
    acc_user.fname,
    acc_user.lname,
    acc_user.email,
    acc_user.phone,
    acc_user.user_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var logoutUser = (callback) => {
  return passQuery(query, values);
}

var getUser = (userId, callback) => {
  query = "SELECT * FROM users WHERE user_id=?";
  values = [
    userId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0].fname);
    return callback(setUserData(result));
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setUserData(data){
  user.userUserId = data[0].user_id;
  user.userFname = data[0].fname;
  user.userLname = data[0].lname;
  user.userEmail = data[0].email;
  user.userPhone = data[0].phone;
  user.userPassword = data[0].password;
  user.userActivation = data[0].activation;
  user.userDateTime = data[0].date_time;
  return user;
}

module.exports = { createUser, authUser, updateUser, getUser }
