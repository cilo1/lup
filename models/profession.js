var db = require('../databases/mysql_db');
var profession = require('../data/profession');

var query = "";
var values = [];
var dateTime = new Date();

var createProfession = function(new_profession, callback){
  query = "INSERT INTO professions VALUES(null,?,?,?)";
  values = [
    new_profession.name,
    new_profession.description,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateProfession = (profession, callback) => {
  query = "UPDATE professions set name=?, description=? WHERE profession_id=?";
  values = [
    profession.name,
    profession.description,
    profession.profession_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getProfession = (professionId, callback) => {
  query = "SELECT * FROM professions WHERE profession_id=?";
  values = [
    professionId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setProfessionData(result));
  });
}

var getProfessions = (callback) => {
  query = "SELECT * FROM professions";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteProfession = (professionId, callback) => {
  query = "DELETE FROM professions WHERE profession_id=?";
  values = [
    professionId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db"); 
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setProfessionData(data){
  profession.professionProfessionId = data[0].profession_id;
  profession.professionName = data[0].name;
  profession.professionDescription = data[0].description;
  profession.professionDateTime = data[0].date_time;
  return profession;
}

module.exports = {
  createProfession,
  updateProfession,
  getProfession,
  getProfessions,
  deleteProfession
}
