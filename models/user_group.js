var db = require('../databases/mysql_db');
var userGroup = require('../data/user_group');

var query = "";
var values = [];
var dateTime = new Date();

var createUserGroup = function(new_user_group, callback){
  query = "INSERT INTO users_groups VALUES(null,?,?,?)";
  values = [
    new_user_group.user_id,
    new_user_group.group_id,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateUserGroup = (userGroup, callback) => {
  query = "UPDATE users_groups set user_id=?, group_id=? WHERE user_group_id=?";
  values = [
    userGroup.user_id,
    userGroup.group_id,
    userGroup.user_group_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getUserGroup = (userGroupId, callback) => {
  query = "SELECT * FROM users_groups WHERE user_group_id=?";
  values = [
    userGroupId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setUserGroupData(result));
  });
}

var getUserGroups = (callback) => {
  query = "SELECT * FROM users_groups";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteUserGroup = (userGroupId, callback) => {
  query = "DELETE FROM users_groups WHERE user_group_id=?";
  values = [
    userGroupId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setUserGroupData(data){
  userGroup.userGroupId = data[0].user_group_id;
  userGroup.userGroupUserId = data[0].user_id;
  userGroup.userGroupGroupId = data[0].group_id;
  userGroup.userGroupDateTime = data[0].date_time;
  return userGroup;
}

module.exports = {
  createUserGroup,
  updateUserGroup,
  getUserGroup,
  getUserGroups,
  deleteUserGroup
}
