var db = require('../databases/mysql_db');
var userInterest = require('../data/user_interest');

var query = "";
var values = [];
var dateTime = new Date();

var createUserInterest = function(new_user_interest, callback){
  query = "INSERT INTO users_interests VALUES(null,?,?,?)";
  values = [
    new_user_interest.user_id,
    new_user_interest.profession_id,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateUserInterest = (userInterest, callback) => {
  query = "UPDATE users_interests set user_id=?, profession_id=? WHERE user_interest_id=?";
  values = [
    userInterest.user_id,
    userInterest.profession_id,
    userInterest.user_interest_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getUserInterest = (userInterestId, callback) => {
  query = "SELECT * FROM users_interests WHERE user_interest_id=?";
  values = [
    userInterestId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setUserInterestData(result));
  });
}

var getUserInterests = (callback) => {
  query = "SELECT * FROM users_interests";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteUserInterest = (userInterestId, callback) => {
  query = "DELETE FROM users_interests WHERE user_interest_id=?";
  values = [
    userInterestId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setUserInterestData(data){
  userInterest.userInterestId = data[0].user_interest_id;
  userInterest.userInterestUserId = data[0].user_id;
  userInterest.userInterestProfessionId = data[0].profession_id;
  userInterest.userInterestDateTime = data[0].date_time;
  return userInterest;
}

module.exports = {
  createUserInterest,
  updateUserInterest,
  getUserInterest,
  getUserInterests,
  deleteUserInterest
}
