var db = require('../databases/mysql_db');
var incident = require('../data/incident');

var query = "";
var values = [];
var dateTime = new Date();

var createIncident = function(new_incident, callback){
  query = "INSERT INTO incidents VALUES(null,?,?,?,?)";
  values = [
    new_incident.victim_id,
    new_incident.accussed_id,
    new_incident.report_id,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateIncident = (incident, callback) => {
  query = "UPDATE incidents set victim_id=?, accussed_id=?, report_id=? WHERE incident_id=?";
  values = [
    incident.victim_id,
    incident.accussed_id,
    incident.report_id,
    incident.incident_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getIncident = (incidentId, callback) => {
  query = "SELECT * FROM incidents WHERE incident_id=?";
  values = [
    incidentId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setIncidentData(result));
  });
}

var getIncidents = (callback) => {
  query = "SELECT * FROM incidents";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteIncident = (incidentId, callback) => {
  query = "DELETE FROM incidents WHERE incident_id=?";
  values = [
    incidentId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setIncidentData(data){
  incident.incidentIncidentId = data[0].incident_id;
  incident.incidentVictimId = data[0].victim_id;
  incident.incidentAccussedId = data[0].accussed_id;
  incident.incidentReportId = data[0].report_id;
  incident.incidentDateTime = data[0].date_time;
  return incident;
}

module.exports = {
  createIncident,
  updateIncident,
  getIncident,
  getIncidents,
  deleteIncident
}
