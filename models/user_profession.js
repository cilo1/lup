var db = require('../databases/mysql_db');
var userProfession = require('../data/user_profession');

var query = "";
var values = [];
var dateTime = new Date();

var createUserProfession = function(new_user_profession, callback){
  query = "INSERT INTO users_professions VALUES(null,?,?,?)";
  values = [
    new_user_profession.user_id,
    new_user_profession.profession_id,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateUserProfession = (userProfession, callback) => {
  query = "UPDATE users_professions set user_id=?, profession_id=? WHERE user_profession_id=?";
  values = [
    userProfession.user_id,
    userProfession.profession_id,
    userProfession.user_profession_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getUserProfession = (userProfessionId, callback) => {
  query = "SELECT * FROM users_professions WHERE user_profession_id=?";
  values = [
    userProfessionId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setUserProfessionData(result));
  });
}

var getUserProfessions = (callback) => {
  query = "SELECT * FROM users_professions";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteUserProfession = (userProfessionId, callback) => {
  query = "DELETE FROM users_professions WHERE user_profession_id=?";
  values = [
    userProfessionId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setUserProfessionData(data){
  userProfession.userProfessionId = data[0].user_profession_id;
  userProfession.userProfessionUserId = data[0].user_id;
  userProfession.userProfessionProfessionId = data[0].profession_id;
  userProfession.userProfessionDateTime = data[0].date_time;
  return userProfession;
}

module.exports = {
  createUserProfession,
  updateUserProfession,
  getUserProfession,
  getUserProfessions,
  deleteUserProfession
}
