var db = require('../databases/mysql_db');
var userPairing = require('../data/user_pairing');

var query = "";
var values = [];
var dateTime = new Date();

var createUserPairing = function(new_user_pairing, callback){
  query = "INSERT INTO users_pairings VALUES(null,?,?,?)";
  values = [
    new_user_pairing.group_id,
    new_user_pairing.pairing,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateUserPairing = (userPairing, callback) => {
  query = "UPDATE users_pairings set group_id=?, pairing=? WHERE user_pairing_id=?";
  values = [
    userPairing.group_id,
    userPairing.pairing,
    userPairing.user_pairing_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getUserPairing = (userPairingId, callback) => {
  query = "SELECT * FROM users_pairings WHERE user_pairing_id=?";
  values = [
    userPairingId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setUserPairingData(result));
  });
}

var getUserPairings = (callback) => {
  query = "SELECT * FROM users_pairings";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteUserPairing = (userPairingId, callback) => {
  query = "DELETE FROM users_pairings WHERE user_pairing_id=?";
  values = [
    userPairingId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setUserPairingData(data){
  userPairing.userPairingId = data[0].user_pairing_id;
  userPairing.userPairingGroupId = data[0].group_id;
  userPairing.userPairingParing = data[0].pairing;
  userPairing.userPairingDateTime = data[0].date_time;
  return userPairing;
}

module.exports = {
  createUserPairing,
  updateUserPairing,
  getUserPairing,
  getUserPairings,
  deleteUserPairing
}
