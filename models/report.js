var db = require('../databases/mysql_db');
var report = require('../data/report');

var query = "";
var values = [];
var dateTime = new Date();

var createReport = function(new_report, callback){
  query = "INSERT INTO reports VALUES(null,?,?,?)";
  values = [
    new_report.item,
    new_report.description,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateReport = (report, callback) => {
  query = "UPDATE reports set item=?, description=? WHERE report_id=?";
  values = [
    report.item,
    report.description,
    report.report_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getReport = (reportId, callback) => {
  query = "SELECT * FROM reports WHERE report_id=?";
  values = [
    reportId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setReportData(result));
  });
}

var getReports = (callback) => {
  query = "SELECT * FROM reports";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteReport = (reportId, callback) => {
  query = "DELETE FROM reports WHERE report_id=?";
  values = [
    reportId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setReportData(data){
  report.reportReportId = data[0].report_id;
  report.reportItem = data[0].item;
  report.reportDescription = data[0].description;
  report.reportDateTime = data[0].date_time;
  return report;
}

module.exports = {
  createReport,
  updateReport,
  getReport,
  getReports,
  deleteReport
}
