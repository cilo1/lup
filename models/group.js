var db = require('../databases/mysql_db');
var group = require('../data/group');

var query = "";
var values = [];
var dateTime = new Date();

var createGroup = function(new_group, callback){
  query = "INSERT INTO groups VALUES(null,?,?,?,?,?,?,?)";
  values = [
    new_group.profession_id,
    new_group.name,
    new_group.description,
    new_group.max_total,
    new_group.current_total,
    new_group.availability,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateGroup = (group, callback) => {
  query = "UPDATE groups set profession_id=?, name=?, description=?,"+
  "max_total=?, current_total=?, availability=?  WHERE group_id=?";
  values = [
    group.profession_id,
    group.name,
    group.description,
    group.max_total,
    group.current_total,
    group.availability,
    group.group_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getGroup = (groupId, callback) => {
  query = "SELECT * FROM groups WHERE group_id=?";
  values = [
    groupId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setGroupData(result));
  });
}

var getGroups = (callback) => {
  query = "SELECT * FROM groups";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteGroup = (groupId, callback) => {
  query = "DELETE FROM groups WHERE group_id=?";
  values = [
    groupId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setGroupData(data){
  group.groupGroupId = data[0].group_id;
  group.groupProfessionId = data[0].profession_id;
  group.groupName = data[0].name;
  group.groupDescription = data[0].description;
  group.groupMaxTotal = data[0].max_total;
  group.groupCurrentTotal = data[0].current_total;
  group.groupAvailability = data[0].availabilityn;
  group.groupDateTime = data[0].date_time;
  return group;
}

module.exports = {
  createGroup,
  updateGroup,
  getGroup,
  getGroups,
  deleteGroup
}
