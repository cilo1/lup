var db = require('../databases/mysql_db');
var appointment = require('../data/appointment');

var query = "";
var values = [];
var dateTime = new Date();

var createAppointment = function(new_appointment, callback){
  query = "INSERT INTO appointments VALUES(null,?,?,?)";
  values = [
    new_appointment.user_pairing_id,
    new_appointment.schedule,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateAppointment = (appointment, callback) => {
  query = "UPDATE appointments set user_pairing_id=?, schedule=? WHERE appointment_id=?";
  values = [
    appointment.user_pairing_id,
    appointment.schedule,
    appointment.appointment_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getAppointment = (appointmentId, callback) => {
  query = "SELECT * FROM appointments WHERE appointment_id=?";
  values = [
    appointmentId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setAppointmentData(result));
  });
}

var getAppointments = (callback) => {
  query = "SELECT * FROM appointments";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteAppointment = (appointmentId, callback) => {
  query = "DELETE FROM appointments WHERE appointment_id=?";
  values = [
    appointmentId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setAppointmentData(data){
  appointment.AppointmentId = data[0].appointment_id;
  appointment.appointmentUserPairingId = data[0].user_pairing_id;
  appointment.appointmentSchedule = data[0].schedule;
  appointment.appointmentDateTime = data[0].date_time;
  return appointment;
}

module.exports = {
  createAppointment,
  updateAppointment,
  getAppointment,
  getAppointments,
  deleteAppointment
}
