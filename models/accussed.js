var db = require('../databases/mysql_db');
var accussed = require('../data/accussed');

var query = "";
var values = [];
var dateTime = new Date();

var createAccussed = function(new_accussed, callback){
  query = "INSERT INTO accussed VALUES(null,?,?)";
  values = [
    new_accussed.user_id,
    dateTime
  ]
  return passQuery(query, values, callback);
}

var updateAccussed = (accussed, callback) => {
  query = "UPDATE accussed set user_id=? WHERE accussed_id=?";
  values = [
    accussed.user_id,
    accussed.accussed_id
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var getAccussed = (accussedId, callback) => {
  query = "SELECT * FROM accussed WHERE accussed_id=?";
  values = [
    accussedId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result[0]);
    return callback(setAccussedData(result));
  });
}

var getAccusseds = (callback) => {
  query = "SELECT * FROM accussed";
  values = []
  passQuery(query, null, function(result){
    if(result === null)
      console.log("No response from db");

    console.log("result: "+result);
    return callback(result);
  });
}

var deleteAccussed = (accussedId, callback) => {
  query = "DELETE FROM accussed WHERE accussed_id=?";
  values = [
    accussedId
  ]
  passQuery(query, values, function(result){
    if(result === null)
      console.log("No response from db");
    console.log("result: "+result);
    return callback(result);
  });
}

function passQuery(query, values, callback){
  db.queryMysql(query, values, function(result){
    console.log("test:"+result);
    return callback(result);
  });
}

function setAccussedData(data){
  accussed.accussedAccussedId = data[0].accussed_id;
  accussed.accussedUserId = data[0].user_id;
  accussed.accussedDateTime = data[0].date_time;
  return accussed;
}

module.exports = {
  createAccussed,
  updateAccussed,
  getAccussed,
  getAccusseds,
  deleteAccussed
}
