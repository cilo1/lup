var report = {
  report_id: 0,
  item: "",
  description: "",
  date_time: ""
}

Object.defineProperty(report, "reportReportId", {
  set: function(report_id){this.report_id = report_id;}
});
Object.defineProperty(report, "reportItem", {
  set: function(item){this.item = item;}
});
Object.defineProperty(report, "reportDescription", {
  set: function(description){this.description = description;}
});
Object.defineProperty(report, "reportDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = report;
