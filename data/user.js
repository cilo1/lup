var user = {
  user_id: 0,
  fname: "",
  lname: "",
  email: "",
  password: "",
  phone: "",
  activation: "",
  date_time: ""
}

Object.defineProperty(user, "userUserId", {
  set: function(user_id){this.user_id = user_id;}
});
Object.defineProperty(user, "userFname", {
  set: function(fname){this.fname = fname;}
});
Object.defineProperty(user, "userLname", {
  set: function(lname){this.lname = lname;}
});
Object.defineProperty(user, "userEmail", {
  set: function(email){this.email = email;}
});
Object.defineProperty(user, "userPassword", {
  set: function(password){this.password = password;}
});
Object.defineProperty(user, "userPhone", {
  set: function(phone){this.phone = phone;}
});
Object.defineProperty(user, "userActivation", {
  set: function(activation){this.activation = activation;}
});
Object.defineProperty(user, "userDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = user;
