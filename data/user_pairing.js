var userPairing = {
  user_pairing_id: 0,
  group_id: 0,
  pairing: "",
  date_time: ""
}

Object.defineProperty(userPairing, "userPairingId", {
  set: function(user_pairing_id){this.user_pairing_id = user_pairing_id;}
});
Object.defineProperty(userPairing, "userPairingGroupId", {
  set: function(group_id){this.group_id = group_id;}
});
Object.defineProperty(userPairing, "userPairingPairing", {
  set: function(pairing){this.pairing = pairing;}
});
Object.defineProperty(userPairing, "userPairingDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = userPairing;
