var group = {
  group_id: 0,
  profession_id: 0,
  name: "",
  description: "",
  max_total: 0,
  current_total: 0,
  availability: "",
  date_time: ""
}

Object.defineProperty(group, "groupGroupId", {
  set: function(group_id){this.group_id = group_id;}
});
Object.defineProperty(group, "groupProfessionId", {
  set: function(profession_id){this.profession_id = profession_id;}
});
Object.defineProperty(group, "groupName", {
  set: function(name){this.name = name;}
});
Object.defineProperty(group, "groupDescription", {
  set: function(description){this.description = description;}
});
Object.defineProperty(group, "groupMaxTotal", {
  set: function(max_total){this.max_total = max_total;}
});
Object.defineProperty(group, "groupCurrentTotal", {
  set: function(current_total){this.current_total = current_total;}
});
Object.defineProperty(group, "groupAvailability", {
  set: function(availability){this.availability = availability;}
});
Object.defineProperty(group, "groupDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = group;
