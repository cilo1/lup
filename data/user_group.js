var userGroup = {
  user_group_id: 0,
  user_id: 0,
  group_id: 0,
  date_time: ""
}

Object.defineProperty(userGroup, "userGroupId", {
  set: function(user_group_id){this.user_group_id = user_group_id;}
});
Object.defineProperty(userGroup, "userGroupUserId", {
  set: function(user_id){this.user_id = user_id;}
});
Object.defineProperty(userGroup, "userGroupGroupId", {
  set: function(group_id){this.group_id = group_id;}
});
Object.defineProperty(userGroup, "userGroupDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = userGroup;
