var accussed = {
  accussed_id: 0,
  user_id: 0,
  date_time: ""
}

Object.defineProperty(accussed, "accussedAccussedId", {
  set: function(accussed_id){this.accussed_id = accussed_id;}
});
Object.defineProperty(accussed, "accussedUserId", {
  set: function(user_id){this.user_id= user_id;}
});
Object.defineProperty(accussed, "accussedDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = accussed;
