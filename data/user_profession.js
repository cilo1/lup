var userProfession = {
  user_profession_id: 0,
  user_id: 0,
  profession_id: 0,
  date_time: ""
}

Object.defineProperty(userProfession, "userProfessionId", {
  set: function(user_profession_id){this.user_profession_id = user_profession_id;}
});
Object.defineProperty(userProfession, "userProfessionUserId", {
  set: function(user_id){this.user_id = user_id;}
});
Object.defineProperty(userProfession, "userProfessionProfessionId", {
  set: function(profession_id){this.profession_id = profession_id;}
});
Object.defineProperty(userProfession, "userProfessionDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = userProfession;
