var appointment = {
  appointment_id: 0,
  user_pairing_id: 0,
  schedule: "",
  date_time: ""
}

Object.defineProperty(appointment, "appointmentId", {
  set: function(appointment_id){this.appointment_id = appointment_id;}
});
Object.defineProperty(appointment, "appointmentUserPairingId", {
  set: function(user_pairing_id){this.user_pairing_id = user_pairing_id;}
});
Object.defineProperty(appointment, "appointmentSchedule", {
  set: function(schedule){this.schedule = schedule;}
});
Object.defineProperty(appointment, "appointmentDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = appointment;
