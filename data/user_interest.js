var userInterest = {
  user_interest_id: 0,
  user_id: 0,
  profession_id: 0,
  date_time: ""
}

Object.defineProperty(userInterest, "userInterestId", {
  set: function(user_interest_id){this.user_interest_id = user_interest_id;}
});
Object.defineProperty(userInterest, "userInterestUserId", {
  set: function(user_id){this.user_id = user_id;}
});
Object.defineProperty(userInterest, "userInterestProfessionId", {
  set: function(profession_id){this.profession_id = profession_id;}
});
Object.defineProperty(userInterest, "userInterestDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = userInterest;
