var profession = {
  profession_id: 0,
  name: "",
  description: "",
  date_time: ""
}

Object.defineProperty(profession, "professionProfessionId", {
  set: function(profession_id){this.profession_id = profession_id;}
});
Object.defineProperty(profession, "professionName", {
  set: function(name){this.name = name;}
});
Object.defineProperty(profession, "professionDescription", {
  set: function(description){this.description = description;}
});
Object.defineProperty(profession, "professionDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = profession;
