var incident = {
  incident_id: 0,
  victim_id: 0,
  accussed_id: 0,
  report_id: 0,
  date_time: ""
}

Object.defineProperty(incident, "incidentIncidentId", {
  set: function(incident_id){this.incident_id = incident_id;}
});
Object.defineProperty(incident, "incidentVictimId", {
  set: function(victim_id){this.victim_id = victim_id;}
});
Object.defineProperty(incident, "incidentAccussedId", {
  set: function(accussed_id){this.accussed_id = accussed_id;}
});
Object.defineProperty(incident, "incidentReportId", {
  set: function(report_id){this.report_id = report_id;}
});
Object.defineProperty(incident, "incidentDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = incident;
