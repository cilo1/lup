var victim = {
  victim_id: 0,
  user_id: 0,
  date_time: ""
}

Object.defineProperty(victim, "victimVictimId", {
  set: function(victim_id){this.victim_id = victim_id;}
});
Object.defineProperty(victim, "victimUserId", {
  set: function(user_id){this.user_id= user_id;}
});
Object.defineProperty(victim, "victimDateTime", {
  set: function(date_time){this.date_time = date_time;}
});

module.exports = victim;
