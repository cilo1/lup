var express = require("express");
var bodyParser = require("body-parser");
var session = require("express-session");
var cookieParser = require("cookie-parser");
var redis = require("redis");
var redisStore = require("connect-redis")(session);
var client = redis.createClient();

var app = express();

app.use(express.static('./public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  secret: "shhhhhh",
  store: new redisStore({ host: 'localhost', port: 6379, client: client,ttl :  260}),
  resave: true,
  saveUninitialized: true
}));

var signupRouter = require("./routers/signup_router");
var loginRouter = require("./routers/login_router");
var profileRouter = require("./routers/profile_router");
var professionRouter = require("./routers/profession_router");
var userProfessionRouter = require("./routers/user_profession_router");
var userInterestRouter = require("./routers/user_interest_router");
var groupRouter = require("./routers/group_router");
var userGroupRouter = require("./routers/user_group_router");
var userPairingRouter = require("./routers/user_pairing_router");
var appointmentRouter = require("./routers/appointment_router");
var reportRouter = require("./routers/report_router");
var incidentRouter = require("./routers/incident_router");
var victimRouter = require("./routers/victim_router");
var accussedRouter = require("./routers/accussed_router");

app.get("", function(req, res){
  let user = req.session.user;
  console.log("Home "+user.fname);
  res.send("Welcome to LUP");
});

app.use("/signup", signupRouter);
app.use("/login", loginRouter);
app.use("/profile", profileRouter);
app.use("/profession", professionRouter);
app.use("/user-profession", userProfessionRouter);
app.use("/user-interest", userInterestRouter);
app.use("/group", groupRouter);
app.use("/user-group", userGroupRouter);
app.use("/user-pairing", userPairingRouter);
app.use("/appointment", appointmentRouter);
app.use("/report", reportRouter);
app.use("/incident", incidentRouter);
app.use("/victim", victimRouter);
app.use("/accussed", accussedRouter);

app.listen(3000);
