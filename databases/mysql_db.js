const mysql = require('mysql');
const config = require('../../node_secret/lup_config');

var mysql_connection = mysql.createConnection({
  host: config.host,
  user: config.user,
  password: config.password,
  database: config.database
});

var dbConnect = function(){
  mysql_connection.connect((err) => {
    if(err)
      console.log("DB connection failed \n Error : " + JSON.stringify(err, undefined, 2));
      return;
    console.log("DB connection succeeded");
  });
}

queryMysql = function(sql, values, callback){
  dbConnect();
  mysql_connection.query(sql,values,(err,rows,fields)=>{
    if(err)
      throw(err);

    //console.log("db: "+rows[0].name);
    return callback(rows);
  });
};

exports.queryMysql = queryMysql;
