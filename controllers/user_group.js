var userGroupModel = require('../models/user_group');
var userGroup = require('../data/user_group');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};
var userGroupId = 1;

var validateUserGroup = (method) => {
  switch(method){
    case 'createUserGroup': {
      return [
        check('group_id', "group doesn't exist").isInt()
      ]
    }
    case 'updateUserGroup': {
      return [
        check('group_id', "group doesn't exist").isInt()
      ]
    }
  }
}

var createUserGroup = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    userGroupModel.createUserGroup(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("group created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("group not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getUserGroup = (req, res) => {
  try{
    userGroupModel.getUserGroup(req.params.id, function(userGroup){
      if(userGroup !== null){
        json_data = {
          'status': 'success',
          'group': {
            'user_group_id': userGroup.user_group_id,
            'user_id': userGroup.user_id,
            'group_id': userGroup.profession_id,
            'date_time': userGroup.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getUserGroups = (req, res) => {
  try{
    userGroupModel.getUserGroups(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'groups': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateUserGroup = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    userGroup.userGroupId = req.body.user_group_id;
    userGroup.userGroupUserId = req.body.user_id;
    userGroup.userGroupGroupId = req.body.group_id;

    userGroupModel.updateUserGroup(userGroup, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("group updated");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("group not updated");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteUserGroup = (req, res) => {
  try{
    userGroupModel.deleteUserGroup(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("group deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("group not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateUserGroup,
  createUserGroup,
  getUserGroup,
  getUserGroups,
  updateUserGroup,
  deleteUserGroup
}
