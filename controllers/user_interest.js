var userInterestModel = require('../models/user_interest');
var userInterest = require('../data/user_interest');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};
var userInterestId = 1;

var validateUserInterest = (method) => {
  switch(method){
    case 'createUserInterest': {
      return [
        check('profession_id', "profession doesn't exist").isInt()
      ]
    }
    case 'updateUserInterest': {
      return [
        check('profession_id', "profession doesn't exist").isInt()
      ]
    }
  }
}

var createUserInterest = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    userInterestModel.createUserInterest(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Interest created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Interest not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getUserInterest = (req, res) => {
  try{
    userInterestModel.getUserInterest(req.params.id, function(userInterest){
      if(userInterest !== null){
        json_data = {
          'status': 'success',
          'interest': {
            'user_interest_id': userInterest.user_Interest_id,
            'user_id': userInterest.user_id,
            'profession_id': userInterest.profession_id,
            'date_time': userInterest.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getUserInterests = (req, res) => {
  try{
    userInterestModel.getUserInterests(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'interests': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateUserInterest = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    userInterest.userInterestId = req.body.user_interest_id;
    userInterest.userInterestUserId = req.body.user_id;
    userInterest.userInterestProfessionId = req.body.profession_id;

    userInterestModel.updateUserInterest(userInterest, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Interest updated");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Interest not updated");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteUserInterest = (req, res) => {
  try{
    userInterestModel.deleteUserInterest(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Interest deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Interest not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateUserInterest,
  createUserInterest,
  getUserInterest,
  getUserInterests,
  updateUserInterest,
  deleteUserInterest
}
