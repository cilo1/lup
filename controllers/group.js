var groupModel = require('../models/group');
var group = require('../data/group');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};

var validateGroup = (method) => {
  switch(method){
    case 'createGroup': {
      return [
        check('name', "group name doesn't exist").isAlpha()
      ]
    }
    case 'updateGroup': {
      return [
        check('name', "group name doesn't exist").isAlpha()
      ]
    }
  }
}

var createGroup = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    groupModel.createGroup(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("group created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("group not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getGroup = (req, res) => {
  try{
    groupModel.getGroup(req.params.id, function(group){
      if(group !== null){
        json_data = {
          'status': 'success',
          'group': {
            'group_id': group.group_id,
            'profession_id': group.profession_id,
            'name': group.name,
            'description': group.description,
            'max_total': group.max_total,
            'current_total': group.current_total,
            'availability': group.availability,
            'date_time': group.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getGroups = (req, res) => {
  try{
    groupModel.getGroups(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'groups': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateGroup = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    group.groupGroupId = req.body.group_id;
    group.groupProfessionId = req.body.profession_id;
    group.groupName = req.body.name;
    group.groupDescription = req.body.description;
    group.groupMaxTotal = req.body.max_total;
    group.groupCurrentTotal = req.body.current_total;
    group.groupAvailability = req.body.availability;

    groupModel.updateGroup(group, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("group updated");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("group not updated");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteGroup = (req, res) => {
  try{
    groupModel.deleteGroup(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("group deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("group not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateGroup,
  createGroup,
  getGroup,
  getGroups,
  updateGroup,
  deleteGroup
}
