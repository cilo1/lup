var victimModel = require('../models/victim');
var victim = require('../data/victim');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};

var validateVictim = (method) => {
  switch(method){
    case 'createVictim': {
      return [
        //check('item', "item doesn't exist").isAlpha()
      ]
    }
    case 'updateVictim': {
      return [
        //check('item', "item doesn't exist").isAlpha()
      ]
    }
  }
}

var createVictim = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    victimModel.createVictim(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("victim created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("victim not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getVictim = (req, res) => {
  try{
    victimModel.getVictim(req.params.id, function(victim){
      if(victim !== null){
        json_data = {
          'status': 'success',
          'victim': {
            'victim_id': victim.victim_id,
            'user_id': victim.user_id,
            'date_time': victim.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getVictims = (req, res) => {
  try{
    victimModel.getVictims(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'reports': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateVictim = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    victim.victimVictimId = req.body.victim_id;
    victim.victimUserId = req.body.user_id;

    victimModel.updateVictim(victim, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("victim created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("victim not created");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteVictim = (req, res) => {
  try{
    victimModel.deleteVictim(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("victim deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("victim not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateVictim,
  createVictim,
  getVictim,
  getVictims,
  updateVictim,
  deleteVictim
}
