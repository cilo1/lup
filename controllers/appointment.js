var appointmentModel = require('../models/appointment');
var appointment = require('../data/appointment');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};

var validateAppointment = (method) => {
  switch(method){
    case 'createAppointment': {
      return [
        //check('schedule', "schedule doesn't exist").isInt()
      ]
    }
    case 'updateAppointment': {
      return [
        //check('schedule', "schedule doesn't exist").isInt()
      ]
    }
  }
}

var createAppointment = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    appointmentModel.createAppointment(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("appointment created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("appointment not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getAppointment = (req, res) => {
  try{
    appointmentModel.getAppointment(req.params.id, function(appointment){
      if(appointment !== null){
        json_data = {
          'status': 'success',
          'appointment': {
            'appointment_id': appointment.appointment_id,
            'user_pairing_id': appointment.user_pairing_id,
            'schedule': appointment.schedule,
            'date_time': appointment.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getAppointments = (req, res) => {
  try{
    appointmentModel.getAppointments(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'appointments': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateAppointment = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    appointment.appointmentId = req.body.appointment_id;
    appointment.appointmentUserPairingId = req.body.user_pairing_id;
    appointment.schedule = req.body.schedule;

    appointmentModel.updateAppointment(appointment, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("appointment updated");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("appointment not updated");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteAppointment = (req, res) => {
  try{
    appointmentModel.deleteAppointment(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("appointment deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("appointment not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateAppointment,
  createAppointment,
  getAppointment,
  getAppointments,
  updateAppointment,
  deleteAppointment
}
