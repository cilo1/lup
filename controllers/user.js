var userModel = require('../models/user');
var user = require('../data/user');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};
var userId = 13;

var validateUser = (method) => {
  switch(method){
    case 'createUser': {
      return [
        check('fname', "Firstname doesn't exist").isAlpha(),
        check('lname', "Lastname doesn't exist").isAlpha(),
        check('email', "Email is invalid").exists().isEmail(),
        check('phone', "Phone is invalid").optional().isInt()
      ]
    }
    case 'loginUser': {
      return [
        check('email', "Email is invalid").exists().isEmail(),
        check('password', "Password is not correct").isLength({min:6})
      ]
    }
    case 'updateUser': {
      return [
        check('fname', "Firstname doesn't exist").isAlpha(),
        check('lname', "Lastname doesn't exist").isAlpha(),
        check('email', "Email is invalid").exists().isEmail(),
        check('phone', "Phone is invalid").optional().isInt()
      ]
    }
  }
}

var createUser = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    userModel.createUser(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("user created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("user not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var loginUser = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }
    userModel.authUser(req.body, function(user){
      if(user !== null){
        json_data = {
          'status': 'success',
          'user': {
            'firstname': user.fname,
            'lastname': user.lname,
            'email': user.email,
            'phone': user.phone,
            'activation': user.activation,
            'date_time': user.date_time
          }
        }
        req.session.user = user;
        console.log("session: "+req.session.user.fname);
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error"+error);
  }
}

var logoutUser = (req, res) => {
  try{

  }catch(error){
    res.send("There was an error "+error);
  }
}

var getUser = (req, res) => {
  try{
    userModel.getUser(userId, function(user){
      if(user !== null){
        json_data = {
          'status': 'success',
          'user': {
            'firstname': user.fname,
            'lastname': user.lname,
            'email': user.email,
            'phone': user.phone
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateUser = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    user.userUserId = userId;
    user.userFname = req.body.fname;
    user.userLname = req.body.lname;
    user.userEmail = req.body.email;
    user.userPhone = req.body.phone;

    userModel.updateUser(user, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("user created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("user not created");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = { validateUser, createUser, loginUser, getUser, updateUser }
