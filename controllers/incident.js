var incidentModel = require('../models/incident');
var incident = require('../data/incident');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};

var validateIncident = (method) => {
  switch(method){
    case 'createIncident': {
      return [
        //check('profession_id', "profession doesn't exist").isInt()
      ]
    }
    case 'updateIncident': {
      return [
      //  check('profession_id', "profession doesn't exist").isInt()
      ]
    }
  }
}

var createIncident = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    incidentModel.createIncident(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("incident created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("incident not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getIncident = (req, res) => {
  try{
    incidentModel.getIncident(req.params.id, function(incident){
      if(incident !== null){
        json_data = {
          'status': 'success',
          'incident': {
            'incident_id': incident.incident_id,
            'victim_id': incident.victim_id,
            'accussed_id': incident.accussed_id,
            'report_id': incident.report_id,
            'date_time': incident.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getIncidents = (req, res) => {
  try{
    incidentModel.getIncidents(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'incidents': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateIncident = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    incident.incidentIncidentId = req.body.incident_id;
    incident.incidentVictimId = req.body.victim_id;
    incident.incidentAccussedId = req.body.accussed_id;
    incident.incidentReportId = req.body.report_id;

    incidentModel.updateIncident(incident, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("incident updated");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("incident not updated");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteIncident = (req, res) => {
  try{
    incidentModel.deleteIncident(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("incident deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("incident not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateIncident,
  createIncident,
  getIncident,
  getIncidents,
  updateIncident,
  deleteIncident
}
