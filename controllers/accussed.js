var accussedModel = require('../models/accussed');
var accussed = require('../data/accussed');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};

var validateAccussed = (method) => {
  switch(method){
    case 'createAccussed': {
      return [
        //check('item', "item doesn't exist").isAlpha()
      ]
    }
    case 'updateAccussed': {
      return [
        //check('item', "item doesn't exist").isAlpha()
      ]
    }
  }
}

var createAccussed = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    accussedModel.createAccussed(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("accussed created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("accussed not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getAccussed = (req, res) => {
  try{
    accussedModel.getAccussed(req.params.id, function(accussed){
      if(accussed !== null){
        json_data = {
          'status': 'success',
          'accussed': {
            'accussed_id': accussed.accussed_id,
            'user_id': accussed.user_id,
            'date_time': accussed.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getAccusseds = (req, res) => {
  try{
    accussedModel.getAccusseds(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'reports': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateAccussed = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    accussed.accussedAccussedId = req.body.accussed_id;
    accussed.accussedUserId = req.body.user_id;

    accussedModel.updateAccussed(accussed, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("accussed created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("accussed not created");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteAccussed = (req, res) => {
  try{
    accussedModel.deleteAccussed(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("accussed deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("accussed not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateAccussed,
  createAccussed,
  getAccussed,
  getAccusseds,
  updateAccussed,
  deleteAccussed
}
