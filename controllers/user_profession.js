var userProfessionModel = require('../models/user_profession');
var userProfession = require('../data/user_profession');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};
var userProfessionId = 1;

var validateUserProfession = (method) => {
  switch(method){
    case 'createUserProfession': {
      return [
        check('profession_id', "profession doesn't exist").isInt()
      ]
    }
    case 'updateUserProfession': {
      return [
        check('profession_id', "profession doesn't exist").isInt()
      ]
    }
  }
}

var createUserProfession = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    userProfessionModel.createUserProfession(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Profession created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Profession not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getUserProfession = (req, res) => {
  try{
    userProfessionModel.getUserProfession(req.params.id, function(userProfession){
      if(userProfession !== null){
        json_data = {
          'status': 'success',
          'profession': {
            'user_profession_id': userProfession.user_profession_id,
            'user_id': userProfession.user_id,
            'profession_id': userProfession.profession_id,
            'date_time': userProfession.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getUserProfessions = (req, res) => {
  try{
    userProfessionModel.getUserProfessions(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'professions': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateUserProfession = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    userProfession.userProfessionId = req.body.user_profession_id;
    userProfession.userProfessionUserId = req.body.user_id;
    userProfession.userProfessionProfessionId = req.body.profession_id;

    userProfessionModel.updateUserProfession(userProfession, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("profession updated");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("profession not updated");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteUserProfession = (req, res) => {
  try{
    userProfessionModel.deleteUserProfession(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Profession deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Profession not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateUserProfession,
  createUserProfession,
  getUserProfession,
  getUserProfessions,
  updateUserProfession,
  deleteUserProfession
}
