var reportModel = require('../models/report');
var report = require('../data/report');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};

var validateReport = (method) => {
  switch(method){
    case 'createReport': {
      return [
        check('item', "item doesn't exist").isAlpha()
      ]
    }
    case 'updateReport': {
      return [
        check('item', "item doesn't exist").isAlpha()
      ]
    }
  }
}

var createReport = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    reportModel.createReport(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("report created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("report not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getReport = (req, res) => {
  try{
    reportModel.getReport(req.params.id, function(report){
      if(report !== null){
        json_data = {
          'status': 'success',
          'report': {
            'report_id': report.report_id,
            'item': report.item,
            'description': report.description,
            'date_time': report.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getReports = (req, res) => {
  try{
    reportModel.getReports(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'reports': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateReport = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    report.reportReportId = req.body.report_id;
    report.reportItem = req.body.item;
    report.reportDescription = req.body.description;

    reportModel.updateReport(report, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("report created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("report not created");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteReport = (req, res) => {
  try{
    reportModel.deleteReport(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("report deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("report not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateReport,
  createReport,
  getReport,
  getReports,
  updateReport,
  deleteReport
}
