var professionModel = require('../models/profession');
var profession = require('../data/profession');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};
var professionId = 13;

var validateProfession = (method) => {
  switch(method){
    case 'createProfession': {
      return [
        check('name', "profession name doesn't exist").isAlpha()
      ]
    }
    case 'updateProfession': {
      return [
        check('name', "profession name doesn't exist").isAlpha()
      ]
    }
  }
}

var createProfession = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    professionModel.createProfession(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Profession created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Profession not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getProfession = (req, res) => {
  try{
    professionModel.getProfession(req.params.id, function(profession){
      if(profession !== null){
        json_data = {
          'status': 'success',
          'profession': {
            'profession_id': profession.profession_id,
            'name': profession.name,
            'description': profession.description,
            'date_time': profession.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getProfessions = (req, res) => {
  try{
    professionModel.getProfessions(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'professions': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateProfession = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    profession.professionProfessionId = req.body.profession_id;
    profession.professionName = req.body.name;
    profession.professionDescription = req.body.description;

    professionModel.updateProfession(profession, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("profession created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("profession not created");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteProfession = (req, res) => {
  try{
    professionModel.deleteProfession(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Profession deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Profession not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateProfession,
  createProfession,
  getProfession,
  getProfessions,
  updateProfession,
  deleteProfession
}
