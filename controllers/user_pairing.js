var userPairingModel = require('../models/user_pairing');
var userPairing = require('../data/user_pairing');
const { check, validationResult } = require('express-validator');
var json_data = {
  'status': 'failed'
};
var userPairingId = 1;

var validateUserPairing = (method) => {
  switch(method){
    case 'createUserPairing': {
      return [
        check('group_id', "group doesn't exist").isInt()
      ]
    }
    case 'updateUserPairing': {
      return [
        check('group_id', "group doesn't exist").isInt()
      ]
    }
  }
}

var createUserPairing = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+errors.array());
      return;
    }

    userPairingModel.createUserPairing(req.body, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Pairing created");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Pairing not created");
    });
  }catch(error){
    console.log("There was an error"+error);
  }
}

var getUserPairing = (req, res) => {
  try{
    userPairingModel.getUserPairing(req.params.id, function(userPairing){
      if(userPairing !== null){
        json_data = {
          'status': 'success',
          'userPairing': {
            'user_pairing_id': userPairing.user_pairing_id,
            'group_id': userPairing.group_id,
            'pairing': userPairing.pairing,
            'date_time': userPairing.date_time
          }
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var getUserPairings = (req, res) => {
  try{
    userPairingModel.getUserPairings(function(result){
      if(result !== null){
        json_data = {
            'status': 'success',
            'userPairings': result
        }
      }
      res.send(json_data);
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

var updateUserPairing = (req, res) => {
  try{
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      console.log("validation errors:"+JSON.stringify(errors.array()[0]));
      return;
    }

    userPairing.userPairingId = req.body.user_pairing_id;
    userPairing.userPairingGroupId = req.body.group_id;
    userPairing.userPairingPairing = req.body.pairing;

    userPairingModel.updateUserPairing(userPairing, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Pairing updated");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Pairing not updated");
    });

  }catch(error){
    res.send("There was an error "+error);
  }
}

var deleteUserPairing = (req, res) => {
  try{
    userPairingModel.deleteUserPairing(req.params.id, function(result){
      console.log("status: "+result);
      if(result.affectedRows === 1){
        console.log("Pairing deleted");
        json_data = {
          'status': 'success'
        }
        res.send(json_data);
      }
      console.log("Pairing not deleted");
    });
  }catch(error){
    res.send("There was an error "+error);
  }
}

module.exports = {
  validateUserPairing,
  createUserPairing,
  getUserPairing,
  getUserPairings,
  updateUserPairing,
  deleteUserPairing
}
