var express = require('express');
var incidentController = require('../controllers/incident');

var incident = express.Router();

incident.get(
  "/",
  incidentController.getIncidents
);

incident.get(
  "/:id",
  incidentController.getIncident
);

incident.post(
  "/",
  incidentController.validateIncident('createIncident'),
  incidentController.createIncident
);

incident.post(
  "/update",
  incidentController.validateIncident('updateIncident'),
  incidentController.updateIncident
);

incident.get(
  "/:id/delete",
  incidentController.deleteIncident
);

module.exports = incident;
