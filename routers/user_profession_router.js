var express = require('express');
var userProfessionController = require('../controllers/user_profession');

var userProfession = express.Router();

userProfession.get(
  "/",
  userProfessionController.getUserProfessions
);

userProfession.get(
  "/:id",
  userProfessionController.getUserProfession
);

userProfession.post(
  "/",
  userProfessionController.validateUserProfession('createUserProfession'),
  userProfessionController.createUserProfession
);

userProfession.post(
  "/update",
  userProfessionController.validateUserProfession('updateUserProfession'),
  userProfessionController.updateUserProfession
);

userProfession.get(
  "/:id/delete",
  userProfessionController.deleteUserProfession
);

module.exports = userProfession;
