var express = require('express');
var appointmentController = require('../controllers/appointment');

var appointment = express.Router();

appointment.get(
  "/",
  appointmentController.getAppointments
);

appointment.get(
  "/:id",
  appointmentController.getAppointment
);

appointment.post(
  "/",
  appointmentController.validateAppointment('createAppointment'),
  appointmentController.createAppointment
);

appointment.post(
  "/update",
  appointmentController.validateAppointment('updateAppointment'),
  appointmentController.updateAppointment
);

appointment.get(
  "/:id/delete",
  appointmentController.deleteAppointment
);

module.exports = appointment;
