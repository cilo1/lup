var express = require('express');
var professionController = require('../controllers/profession');

var profession = express.Router();

profession.get(
  "/",
  professionController.getProfessions
);

profession.get(
  "/:id",
  professionController.getProfession
);

profession.post(
  "/",
  professionController.validateProfession('createProfession'),
  professionController.createProfession
);

profession.post(
  "/update",
  professionController.validateProfession('updateProfession'),
  professionController.updateProfession
);

profession.get(
  "/:id/delete",
  professionController.deleteProfession
);

module.exports = profession;
