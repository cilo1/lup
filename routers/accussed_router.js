var express = require('express');
var accussedController = require('../controllers/accussed');

var accussed = express.Router();

accussed.get(
  "/",
  accussedController.getAccusseds
);

accussed.get(
  "/:id",
  accussedController.getAccussed
);

accussed.post(
  "/",
  accussedController.validateAccussed('createAccussed'),
  accussedController.createAccussed
);

accussed.post(
  "/update",
  accussedController.validateAccussed('updateAccussed'),
  accussedController.updateAccussed
);

accussed.get(
  "/:id/delete",
  accussedController.deleteAccussed
);

module.exports = accussed;
