var express = require('express');
var victimController = require('../controllers/victim');

var victim = express.Router();

victim.get(
  "/",
  victimController.getVictims
);

victim.get(
  "/:id",
  victimController.getVictim
);

victim.post(
  "/",
  victimController.validateVictim('createVictim'),
  victimController.createVictim
);

victim.post(
  "/update",
  victimController.validateVictim('updateVictim'),
  victimController.updateVictim
);

victim.get(
  "/:id/delete",
  victimController.deleteVictim
);

module.exports = victim;
