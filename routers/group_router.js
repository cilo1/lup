var express = require('express');
var groupController = require('../controllers/group');

var group = express.Router();

group.get(
  "/",
  groupController.getGroups
);

group.get(
  "/:id",
  groupController.getGroup
);

group.post(
  "/",
  groupController.validateGroup('createGroup'),
  groupController.createGroup
);

group.post(
  "/update",
  groupController.validateGroup('updateGroup'),
  groupController.updateGroup
);

group.get(
  "/:id/delete",
  groupController.deleteGroup
);

module.exports = group;
