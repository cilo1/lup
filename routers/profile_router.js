var express = require('express');
var userController = require('../controllers/user');

var profile = express.Router();

profile.get(
  "/",
  userController.getUser
);

profile.post(
  "/",
  userController.validateUser('updateUser'),
  userController.updateUser
);

module.exports = profile;
