var express = require('express');
var reportController = require('../controllers/report');

var report = express.Router();

report.get(
  "/",
  reportController.getReports
);

report.get(
  "/:id",
  reportController.getReport
);

report.post(
  "/",
  reportController.validateReport('createReport'),
  reportController.createReport
);

report.post(
  "/update",
  reportController.validateReport('updateReport'),
  reportController.updateReport
);

report.get(
  "/:id/delete",
  reportController.deleteReport
);

module.exports = report;
