var express = require('express');
var userPairingController = require('../controllers/user_pairing');

var userPairing = express.Router();

userPairing.get(
  "/",
  userPairingController.getUserPairings
);

userPairing.get(
  "/:id",
  userPairingController.getUserPairing
);

userPairing.post(
  "/",
  userPairingController.validateUserPairing('createUserPairing'),
  userPairingController.createUserPairing
);

userPairing.post(
  "/update",
  userPairingController.validateUserPairing('updateUserPairing'),
  userPairingController.updateUserPairing
);

userPairing.get(
  "/:id/delete",
  userPairingController.deleteUserPairing
);

module.exports = userPairing;
