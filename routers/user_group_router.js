var express = require('express');
var userGroupController = require('../controllers/user_group');

var userGroup = express.Router();

userGroup.get(
  "/",
  userGroupController.getUserGroups
);

userGroup.get(
  "/:id",
  userGroupController.getUserGroup
);

userGroup.post(
  "/",
  userGroupController.validateUserGroup('createUserGroup'),
  userGroupController.createUserGroup
);

userGroup.post(
  "/update",
  userGroupController.validateUserGroup('updateUserGroup'),
  userGroupController.updateUserGroup
);

userGroup.get(
  "/:id/delete",
  userGroupController.deleteUserGroup
);

module.exports = userGroup;
