var express = require('express');
var userController = require('../controllers/user');

var login = express.Router();

login.post(
  "/",
  userController.validateUser('loginUser'),
  userController.loginUser
);

module.exports = login;
