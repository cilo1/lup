var express = require('express');
var userInterestController = require('../controllers/user_interest');

var userInterest = express.Router();

userInterest.get(
  "/",
  userInterestController.getUserInterests
);

userInterest.get(
  "/:id",
  userInterestController.getUserInterest
);

userInterest.post(
  "/",
  userInterestController.validateUserInterest('createUserInterest'),
  userInterestController.createUserInterest
);

userInterest.post(
  "/update",
  userInterestController.validateUserInterest('updateUserInterest'),
  userInterestController.updateUserInterest
);

userInterest.get(
  "/:id/delete",
  userInterestController.deleteUserInterest
);

module.exports = userInterest;
