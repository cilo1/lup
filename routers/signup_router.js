var express = require('express');
var userController = require('../controllers/user');

var signup = express.Router();

signup.post(
  "/",
  userController.validateUser('createUser'),
  userController.createUser
);

module.exports = signup;
